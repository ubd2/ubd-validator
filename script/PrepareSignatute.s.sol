// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {UBDValidator}  from "../src/UBDValidator.sol";
import { ECDSA } from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import { MessageHashUtils } from "@openzeppelin/contracts/utils/cryptography/MessageHashUtils.sol";



contract PrepareDigest is Script {
    using stdJson for string;
    using ECDSA for bytes32;
    using MessageHashUtils for bytes32;
    UBDValidator public validator;
    uint256 signerPrivateKey = 0x706de24fb39f84ddb0e5dc540f7c92806599b728144989041982f1514eb053e6;
    address signer_address = 0x62078A97CeE6727730C8d56c3386B8Ee219aC9e5;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, native balnce %s", msg.sender, msg.sender.balance);
        validator  = UBDValidator(0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF);

        bytes32 digest = keccak256(abi.encodePacked(
            uint256(100000),     // usdtAmount, 
            uint256(0),          // ubdAmount , 
            0x71373aa15b6d537E70138A39709B50e32C3660Ec, //claimerAddress, 
            validator.userNonce(0x71373aa15b6d537E70138A39709B50e32C3660Ec) + 1 // nonce
        )).toEthSignedMessageHash(); 

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(signerPrivateKey, digest);
        bytes memory signature = abi.encodePacked(r, s, v); 

        validator.claimReward(uint256(100000), uint256(0), 1, signature);
    }
}
