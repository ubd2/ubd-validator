// SPDX-License-Identifier: MIT
pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract UBDValidator is Ownable, Pausable {
    using SafeERC20 for IERC20;

    IERC20 public immutable _USDT;
    IERC20 public immutable _UBD;
    address public signer;

    mapping (address => uint32) public userNonce;
    mapping (bytes32 => bool) public isAlreadyPassed;

    event Withdraw(address indexed _user, uint256 _amount, uint256 _ubdAmount, uint32 _nonce);

    error BadSignature();
    error AlreadyPassed(bytes32);
    error BadNonce(uint32);

    constructor(address _signer, address _usdt, address _ubd) Ownable(msg.sender) {
        _USDT = IERC20(_usdt);
        _UBD = IERC20(_ubd);
        signer = _signer;
    }

    function claimReward(uint256 usdtAmount, uint256 ubdAmount, uint32 nonce, bytes memory signature) external whenNotPaused {
        userNonce[msg.sender]++;

        bytes32 message = _prefixed(keccak256(abi.encodePacked(usdtAmount, ubdAmount ,msg.sender, nonce)));

        if (_recoverSigner(message, signature) != signer) { revert BadSignature(); }
        if(userNonce[msg.sender] != nonce) { revert BadNonce(nonce); }
        if (isAlreadyPassed[message]) { revert AlreadyPassed(message); }

        isAlreadyPassed[message] = true;

        _USDT.safeTransfer(msg.sender, usdtAmount);
        _UBD.safeTransfer(msg.sender, ubdAmount);
        emit Withdraw(msg.sender, usdtAmount, ubdAmount, nonce);
    }

    function checkSignature(uint256 usdtAmount, uint256 ubdAmount, uint32 nonce, address _user) external view returns (bool) {
        bytes32 message = _prefixed(keccak256(abi.encodePacked(usdtAmount, ubdAmount , _user, nonce)));
        return  isAlreadyPassed[message];
    }

    function setSigner(address _newSigner) external onlyOwner {
        signer = _newSigner;
    }

    function pause() external onlyOwner {
        _pause();
    }

    function unpause() external onlyOwner {
        _unpause();
    }

    function _splitSignature(bytes memory sig)
        internal
        pure
        returns (uint8 v, bytes32 r, bytes32 s)
    {
        require(sig.length == 65);

        assembly {
            // first 32 bytes, after the length prefix.
            r := mload(add(sig, 32))
            // second 32 bytes.
            s := mload(add(sig, 64))
            // final byte (first byte of the next 32 bytes).
            v := byte(0, mload(add(sig, 96)))
        }

        return (v, r, s);
    }

    function _recoverSigner(bytes32 message, bytes memory sig)
        internal
        pure
        returns (address)
    {
        (uint8 v, bytes32 r, bytes32 s) = _splitSignature(sig);

        return ecrecover(message, v, r, s);
    }

    /// builds a prefixed hash to mimic the behavior of eth_sign.
    function _prefixed(bytes32 hash) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", hash));
    }
}