# UBD Network Validator
Authorized reward manager


## Usage

### Build

```shell
$ forge build
```

### Test

```shell
$ forge test
```

### Gas Snapshots

```shell
$ forge snapshot
```


### Deploy

### Deploy 
#### Mainnet
```shell
$ forge script script/Deploy.s.sol:DeployScript --rpc-url mainnet  --account ubd_deployer --sender 0x71373aa15b6d537E70138A39709B50e32C3660Ec --broadcast --verify
```

**UBDValidator**  
https://etherscan.io/address/0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF#code

### Cast Usefull comands 

```shell
## Latest block number
$ cast block --rpc-url mainnet | grep number


$ forge script script/PrepareSignatute.s.sol:PrepareDigest --account ubd_deployer --sender 0x71373aa15b6d537E70138A39709B50e32C3660Ec 

$ cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1

$ cast keccak $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1)

$ cast keccak $(cast abi-encode "g(string,bytes32)" "\x19Ethereum Signed Message:\n32" $(cast keccak $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1)))

$ # manual sign gen examples
$ cast wallet sign  $(cast keccak $(cast abi-encode "g(bytes32,bytes32)" "\x19Ethereum Signed Message:\n32" $(cast keccak $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1)))) --account ubd_deployer --no-hash

$ cast wallet sign  $(cast keccak $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1)) --account ubd_deployer --no-hash

$ cast wallet sign  $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1) --account ubd_deployer 

$ cast wallet sign  $(cast abi-encode "g(uint256,uint256,address,uint32)" 100000 0 0x71373aa15b6d537E70138A39709B50e32C3660Ec 1) --account ubd_deployer

 
$ # Look for  signature in PrepareSignature script
$ cast send 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "claimReward(uint256,uint256,uint32,bytes)" 100000 0 1 0xb8dfaf279e750a98ff562f219b7bbd6d730a70dfbd3bf08571b7a6e72e2b11535a8735001376eaccf0515ec1b9e078d8358c9d04ce483b012afeefbf63b539271c  --rpc-url mainnet --account ubd_deployer


$ cast call 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "userNonce(address)" 0x71373aa15b6d537E70138A39709B50e32C3660Ec

$ cast send 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "setSigner(address)" 0x62078A97CeE6727730C8d56c3386B8Ee219aC9e5 --rpc-url mainnet --account ubd_deployer 

$ cast send 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "setSigner(address)" 0x5c6C2D9E324fF674deF8e302c8557a525AF92155 --rpc-url mainnet --account ubd_deployer 

$ cast send 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "setSigner(address)" 0x5c6C2D9E324fF674deF8e302c8557a525AF92155 --rpc-url mainnet --account ubd_deployer 

$ # Transfer ownership to team's multisig
$ cast send 0x6532f9Cc71E41F7238Ba2661948303fdC3E286CF "transferOwnership(address)" 0xE206f8AC6067d8253C57D86ac96A789Cd90ed4D4 --rpc-url mainnet --account ubd_deployer 
$ # https://etherscan.io/tx/0x53e675e6ff89de872f696b8032f152572a5a0ff04db711232c3b62bcef7cac70
```

### Add forge to existing Brownie project
```shell
$ forge init --force
$ forge install OpenZeppelin/openzeppelin-contracts
$ forge buld
```
### First build
```shell
git clone git@gitlab.com:ubd2/ubd-validator.git
git submodule update --init --recursive
```